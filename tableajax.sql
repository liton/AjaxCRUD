-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2016 at 03:18 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_ajax`
--

-- --------------------------------------------------------

--
-- Table structure for table `tableajax`
--

CREATE TABLE IF NOT EXISTS `tableajax` (
`id` int(11) NOT NULL,
  `studentname` varchar(255) NOT NULL,
  `gender` varchar(16) NOT NULL,
  `phone` varchar(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tableajax`
--

INSERT INTO `tableajax` (`id`, `studentname`, `gender`, `phone`) VALUES
(1, '', '0', '0'),
(2, 'Md. Liton Ali', '0', '1751229747'),
(3, 'Md. Liton Ali', 'Male', '1751229747'),
(4, 'Md. Liton Ali', 'Male', '1234'),
(5, 'Lyli', 'Male', '1234'),
(6, 'tedsy female', 'Male', '3222221'),
(7, 'wwww', 'Female', '1111'),
(8, 'www', 'Female', '1'),
(9, 'liton', 'Female', '123445566'),
(10, 'liton', 'Male', '9172'),
(11, 'test ', 'f', '232'),
(12, 'Md. Liton Ali Sarder', 'Male', '23476544'),
(13, 'Md. Liton Ali Sarder', 'Male', '023476544'),
(14, 'Md. Liton Ali Sarder', 'Male', '023476544'),
(15, 'test', 'm', '1'),
(16, 'test', 'm', '1'),
(17, 'test', 'm', '1'),
(18, 'mliton', 'm', '1323232312323'),
(19, 'tttttttt', 'tttttttttttt', '111111111111111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tableajax`
--
ALTER TABLE `tableajax`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tableajax`
--
ALTER TABLE `tableajax`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
